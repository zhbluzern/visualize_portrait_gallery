import pandas as pd
import json

# Read the Excel file into a DataFrame
df = pd.read_excel('workbench/Katalogsaal_01_Frames.xlsx')


# Create an empty dictionary to hold the frame data
frames = {}

# Iterate through each row in the DataFrame to populate the dictionary
for index, row in df.iterrows():
    frame_id = row['FrameID']
    image_name = row['Image']
    frame_label = row["FrameLabel"]
    numOfPortsinRow = row["NumOfPortInRow"]

    top_left = row['top-left'].replace('X: ', '').replace('Y: ', '').split(',')
    top_left_x, top_left_y = map(int, [x.strip() for x in top_left])
    
    bottom_right = row['bottom-right'].replace('X: ', '').replace('Y: ', '').split(',')
    bottom_right_x, bottom_right_y = map(int, [x.strip() for x in bottom_right])
    
    if frame_id not in frames:
        frames[frame_id] = {
            "images": [],
            "portraits": [],
            "frameImage" : f"{frame_id}.jpg",
            "frameLabel" : frame_label,
            "numOfPortsinRow":numOfPortsinRow
        }

    frames[frame_id]['images'].append({
        "imageName":  image_name,  # Change this as per your actual image name
        "coordinates": {
            "topLeft": {"x": top_left_x, "y": top_left_y},
            "bottomRight": {"x": bottom_right_x, "y": bottom_right_y}
        }
    })


    df2 = pd.read_excel('workbench/Portraits_inFrames.xlsx')
    df2 = df2.loc[df2['FrameID'] == frame_id]
    frames[frame_id]['portraits'] = [] #Reset Portraits-List to avoid duplicate entries
    for index2, row2 in df2.iterrows():
        print(row2["QID"])
        top_left = row2['top-left'].replace('X: ', '').replace('Y: ', '').split(',')
        top_left_x, top_left_y = map(int, [x.strip() for x in top_left])
    
        bottom_right = row2['bottom-right'].replace('X: ', '').replace('Y: ', '').split(',')
        bottom_right_x, bottom_right_y = map(int, [x.strip() for x in bottom_right])
        frames[frame_id]['portraits'].append({
            "QID": row2["QID"],
            "portraitFile" : row2["portraitFile"],
            "portraitNr" : str(row2["portraitNr"]),
            "fileUrl" : str(row2["fileUrl"]),
            "coordinates": {
                "topLeft": {"x": top_left_x, "y": top_left_y},
                "bottomRight": {"x": bottom_right_x, "y": bottom_right_y}
            }
        })

print(frames)
# Write the dictionary to a JSON file
with open('public/src/frames.json', 'w') as f:
    json.dump(frames, f, indent=4)
