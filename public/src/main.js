let images = [
  'Katalogsaal_00.jpg',
  'Katalogsaal_01.jpg',
  'Katalogsaal_02.jpg',
  'Katalogsaal_03.jpg',
  'Katalogsaal_04.jpg',
  'Katalogsaal_05.jpg',
  'Katalogsaal_06.jpg'
];

let currentIndex = 1;
let history = [currentIndex]; // A history array to keep track of views for undo/redo

function move(direction) {
  let imgElement = document.getElementById('currentView');

  if (direction === 'forward') {
    // Move forward logic
    currentIndex = (currentIndex + 1) % images.length;
    history.push(currentIndex); // add to history
  } else if (direction === 'backward') {
    // Move backward logic
    currentIndex = (currentIndex - 1 + images.length) % images.length;
    history.push(currentIndex); // add to history
  } else if (direction === 'right') {
    // Turn right logic
    currentIndex = (currentIndex + 2) % images.length; // For illustration, advances by 2 images
  } else if (direction === 'left') {
    // Turn left logic
    currentIndex = (currentIndex - 2 + images.length) % images.length; // Goes back by 2 images
  } else if (direction === 'redo') {
    if (history.length > 1) { // if we have more than one state in history
      history.pop(); // remove the last state
      currentIndex = history[history.length - 1]; // go to the previous state
    }
  } else if (direction === 'undo') {
    // Your undo logic here, e.g., go back to some previous index in a history array
    if (history.length > 0) {
      currentIndex = history.pop(); // go to the last state and remove it from history
    }
  }
  
  imgElement.src = "../images/" + images[currentIndex];
  updateOverlays(images[currentIndex]);
}

function showInfo() {
  const infoElement = document.getElementById("imageInfo");
  if (infoElement.style.display === "none") {
    infoElement.style.display = "block";
  } else {
    infoElement.style.display = "none";
  }
}

async function updateOverlays() {
  let frameData = {};

  // Fetch JSON data
  try {
    const response = await fetch('frames.json'); // Change the URL to match your setup
    if (!response.ok) {
      throw new Error('Failed to fetch JSON');
    }
    frameData = await response.json();
  } catch (error) {
    console.error('Error fetching JSON:', error);
    return;
  }

  const overlayContainer = document.getElementById("overlayContainer");
  overlayContainer.innerHTML = ""; // Clear previous overlays

  const currentImage = document.getElementById("currentView").src.split("/").pop();
  //console.log(currentImage);
  for (const [frameID, details] of Object.entries(frameData)) {
      const imageDetails = details.images.find(img => img.imageName === currentImage);
      //console.log(imageDimensions[currentImage]);
      //console.log(details);
      if (imageDetails) {
          const coordinates = imageDetails.coordinates;
          //console.log(coordinates)
          const frameDiv = document.createElement("div");
          frameDiv.className = "frameOverlay";
          frameDiv.innerText = details.frameLabel;
          frameDiv.style.paddingTop = (coordinates.bottomRight.y - coordinates.topLeft.y )/2 + "px"; 
          frameDiv.style.left = coordinates.topLeft.x + "px";
          frameDiv.style.top = coordinates.topLeft.y + "px";
          frameDiv.style.width = (coordinates.bottomRight.x - coordinates.topLeft.x)  + "px";
          frameDiv.style.height = (coordinates.bottomRight.y - coordinates.topLeft.y) + "px"; 

          frameDiv.setAttribute("data-frame", frameID);
          frameDiv.addEventListener("click", () => {
              openModal(frameID, details);
          });
  

  
          overlayContainer.appendChild(frameDiv);
      }
  }
  
}
let currentPortraitIndex = 0; // Initialize to the first portrait
let currentPortraitData = [];
 function openModal(frameID, frameData) {
  const portraits = frameData.portraits;
  const modalContent = document.getElementById('portraitContainer'); // A container inside the modal for the portraits
  
  // Clear existing content
  modalContent.innerHTML = '';
  //document.getElementById("wikidataInfo").style.display = "none";
  console.log(frameData.numOfPortsinRow)
  let columnStr = "";
  for (let i = 0; i <frameData.numOfPortsinRow; i++) {
    columnStr = columnStr+ "auto ";
  }
  modalContent.style.gridTemplateColumns = columnStr

  portraits.forEach((portrait) => {
    const coordinates = portrait.coordinates;
    const portraitDiv = document.createElement('div');
    /*portraitDiv.style.position = 'absolute';
    portraitDiv.style.left = coordinates.topLeft.x-600 + 'px';
    portraitDiv.style.top = coordinates.topLeft.y + 'px';*/
    
    /* portraitDiv.style.width = (coordinates.bottomRight.x - coordinates.topLeft.x) + 'px';
    portraitDiv.style.height = (coordinates.bottomRight.y - coordinates.topLeft.y) + 'px'; */
    portraitDiv.style.width = "125px";
    portraitDiv.style.height = "190px";
    portraitDiv.style.overflow = "hidden";

    //portraitDiv.innerHTML = `<img class="portrait" src="../images/${portrait.portraitFile}" alt="Portrait">`;
    portraitDiv.innerHTML = `<img class="portrait" src="${portrait.fileUrl.replace("/max/","/240,380/")}" alt="Portrait">`;
    portraitDiv.setAttribute("data-QID", portrait.QID); // Store QID in data-attribute
    portraitDiv.setAttribute("class","portrait");
    portraitDiv.setAttribute("data-index", currentPortraitIndex);
    currentPortraitIndex++;

    
    portraitDiv.addEventListener("click", async function() {
      const QID = this.getAttribute("data-QID");
      const portraitIndex = this.getAttribute("data-index");
      const infoBox = await getInfobox(portrait)
      
      if (infoBox) {
        //openWikidataModal("../images/"+portrait.portraitFile,portraits,portraitIndex);
        openWikidataModal(portrait.fileUrl,portraits,portraitIndex);
      }
    });


    modalContent.appendChild(portraitDiv  );
  });

// Open the modal
const modal = document.getElementById("frameModal");
modal.style.display = "block";
}


function closeModal() {
  const modal = document.getElementById('frameModal');
  modal.style.display = "none";
}


function closeModal() {
  const modal = document.getElementById('frameModal');
  modal.style.display = "none";
}

async function initializePage() {
  // Load initial data and setup
  await updateOverlays();  // Ensure this function is async and fetches JSON data
}

function openWikidataModal(portraitSrc, portraits, portraitIndex) {
  console.log(portraits);
  console.log(portraitIndex);
  currentPortraitIndex = portraitIndex;
  currentPortraitData = portraits;
  const largePortrait = document.getElementById('largePortrait');
  // Hide frameModal
  document.getElementById('frameModal').style.display = 'none';
  //Hide Buttons of Groundlayer
  buttons = document.getElementsByClassName("imageButton");
  console.log(buttons);
  for(var i = 0; i < buttons.length; i++){
    buttons[i].style.display = 'none';
  }

  // Populate the modal
  largePortrait.src = portraitSrc;

  // Show the second modal
  const wikidataModal = document.getElementById('wikidataModal');
  wikidataModal.style.display = "block";
}

function closeWikidataModal() {
  // Hide wikidataModal
  document.getElementById('wikidataModal').style.display = 'none';
  // Show frameModal again
  document.getElementById('frameModal').style.display = 'block';
  //Show Buttons of Groundlayer
  buttons = document.getElementsByClassName("imageButton");
  console.log(buttons);
  for(var i = 0; i < buttons.length; i++){
    buttons[i].style.display = 'block';
  }
}


async function updatePortrait(portraitData) {
  const portrait = portraitData[currentPortraitIndex];
  // Fetch and update Wikidata information based on portrait.QID
  await getInfobox(portrait)    
}

async function getInfobox(portrait){
  let loadingTimeout;

  // Show the loading spinner
  // Schedule the spinner to show after 300ms
  loadingTimeout = setTimeout(() => {
    document.getElementById('loading-spinner').style.display = 'block';
  }, 300);


  try {
    // Fetch Wikidata information
    console.log(portrait)

    let vorgangsTitel = portrait.portraitFile.replace("_cropped.jpg", "");
    const metsURL = 'https://zentralgut.ch/sourcefile?id='+vorgangsTitel;
    const zentralGutData = await fetchFromMETS(metsURL) // Usage
    const response = await runSPARQLQuery(portrait.QID);
    const WDresult = await response.results.bindings[0];
    const imgElement = await loadImageAsync(portrait.fileUrl);
    console.log(response.results.bindings);
    dob = (transformIsoDate(WDresult?.dob?.value, WDresult?.dobPrecision?.value));
    dod = (transformIsoDate(WDresult?.dod?.value, WDresult?.dodPrecision?.value));
    console.log(WDresult);
    
    let bioInfo = "";
    if (typeof dob !== "undefined"){
      bioInfo = bioInfo + `Geboren: ${dob}`;
      if (typeof WDresult?.poBLabel?.value !== "undefined"){  bioInfo = bioInfo + `, ${WDresult?.poBLabel?.value}`; }
    }
    if (typeof dod !== "undefined"){
      if (bioInfo != "") { bioInfo = bioInfo + "</br>"; }
      bioInfo = bioInfo + `Gestorben: ${dod}`;
      if (typeof WDresult?.poDLabel?.value !== "undefined"){  bioInfo = bioInfo + `, ${WDresult?.poDLabel?.value}`; }
    }
    if (typeof WDresult?.hlsId?.value !== "undefined") {
      hlsIdUrl = " &bull; "+(`<a href="https://hls-dhs-dss.ch/de/articles/${WDresult.hlsId.value}">Historisches Lexikon der Schweiz</a>`);
    }
    else { hlsIdUrl = ""; }
    if (typeof WDresult?.gndId?.value !== "undefined") {
      rzsUrl = " &bull; "+(`<a href="https://rzs.swisscovery.slsp.ch/discovery/search?query=any,contains,${WDresult.gndId?.value}&tab=41SLSP_RZS_MyInst_and_CI&search_scope=MyInst_and_CI&vid=41SLSP_RZS:VU15&offset=0">Suche in RZS Swisscovery</a>`);
    }
    else { rzsUrl = ""; }
    if (typeof WDresult?.wpPage?.value !== "undefined") {
      wpUrl = " &bull; "+(`<a href="${WDresult.wpPage?.value}">Wikipedia</a>`);
    }
    else { wpUrl = ""; }  


    let relPersInfo = "<h4>Verbindungen zu anderen Personen in der Porträtgalerie</h4>";
    let relPersLen = relPersInfo.length;
    let relPersonQids = [];
    let relFamPersInfo = "<h5>Verwandte Personen</h5><small>";
    let relFramPersLen = relFamPersInfo.length;
    let relOccupPersoInfo = "<h5>Personen mit gleichem Berufe</h5><small>";
    let relOccupPersLen = relOccupPersoInfo.length;
    let occupI = 0;
    //PersonenInfos sind multiple run through response.results.bindings
    for (const WDresult of response.results.bindings){
      if (typeof WDresult?.childLabel?.value !== "undefined") {
        persQID = WDresult.child?.value.replace("http://www.wikidata.org/entity/","");
        if (!relPersonQids.includes(persQID)){
          if (relFamPersInfo.length != relFramPersLen) { relFamPersInfo = relFamPersInfo+" &bull; "; }
          relFamPersInfo = relFamPersInfo + `<a href="#${persQID}" class="updateInfoBox" data-id="${persQID}">${WDresult.childLabel?.value}</a> (Kind)`;
          relPersonQids.push(persQID);
        }
      }
      if (typeof WDresult?.fatherLabel?.value !== "undefined") {
        persQID = WDresult.father?.value.replace("http://www.wikidata.org/entity/","");
        if (!relPersonQids.includes(persQID)){
          if (relFamPersInfo.length != relFramPersLen) { relFamPersInfo = relFamPersInfo+" &bull; "; }
          relFamPersInfo = relFamPersInfo + `<a href="#${persQID}" class="updateInfoBox" data-id="${persQID}">${WDresult.fatherLabel?.value}</a> (Vater)`;
          relPersonQids.push(persQID);
        }      
      }  
      if (typeof WDresult?.motherLabel?.value !== "undefined") {
        persQID = WDresult.mother?.value.replace("http://www.wikidata.org/entity/","");
        if (!relPersonQids.includes(persQID)){
          if (relFamPersInfo.length != relFramPersLen) { relFamPersInfo = relFamPersInfo+" &bull; "; }
          relFamPersInfo = relFamPersInfo + `<a href="#${persQID}" class="updateInfoBox" data-id="${persQID}">${WDresult.motherLabel?.value}</a> (Mutter)`;
          relPersonQids.push(persQID);
        }            
      }  
      if (typeof WDresult?.relativeLabel?.value !== "undefined") {
        persQID = WDresult.relative?.value.replace("http://www.wikidata.org/entity/","");
        if (!relPersonQids.includes(persQID)){
          if (relFamPersInfo.length != relFramPersLen) { relFamPersInfo = relFamPersInfo+" &bull; "; }
          relFamPersInfo = relFamPersInfo + `<a href="#${persQID}" class="updateInfoBox" data-id="${persQID}">${WDresult.relativeLabel?.value}</a> (${WDresult.kinshipLabel?.value})`;
          relPersonQids.push(persQID);
        }            
      }  
        
      if (typeof WDresult?.occupCollLabel?.value !== "undefined") {
        persQID = WDresult.occupColl?.value.replace("http://www.wikidata.org/entity/","");
        if (occupI > 2) { occupStyle = "display:none;"} else {  occupStyle = ""; }
        occupI++;
        if (!relPersonQids.includes(persQID)){
          relOccupPersoInfo = relOccupPersoInfo + `<span class='occups' style="${occupStyle}">`;
          if (occupI > 1) { relOccupPersoInfo = relOccupPersoInfo+" &bull; "; }
          relOccupPersoInfo = relOccupPersoInfo + `<a href="#${persQID}" class="updateInfoBox" data-id="${persQID}">${WDresult.occupCollLabel?.value}</a> (${WDresult.occupation?.value})`;
          relPersonQids.push(persQID);
          relOccupPersoInfo = relOccupPersoInfo + "</span>";
        }  

      }        
    }
    if (relFamPersInfo.length != relFramPersLen) { relPersInfo = relPersInfo + relFamPersInfo+"</small>"; }
    if (relOccupPersoInfo.length != relOccupPersLen) { 
      relPersInfo = relPersInfo + relOccupPersoInfo; 
      if (occupI > 2 ) {  relPersInfo = relPersInfo + ` <span id="showOccupsLink">[<a href="#" id="showOccups">weitere anzeigen</a>]</span>`;  }
      relPersInfo = relPersInfo +"</small>"; 
    }
    if (relPersInfo.length == relPersLen) { relPersInfo = ""; } 

    if (WDresult && zentralGutData && imgElement) {
        // Update the #wikidataInfo HTML content here based on wikidataInfo
        const wikidataInfoDiv = document.getElementById("wikidataInfo");
        // Generate HTML content for Wikidata information
        const wikidataHTML = `
        <h3 name="${portrait.QID}">${WDresult.label.value} (Porträt Nr. ${portrait.portraitNr})</h3>
          <p>${WDresult.desc.value}</p>
          <p>${bioInfo}</p>
          <blockquote><p>${zentralGutData.text}</p><small>Quelle: <a href="${zentralGutData.href}">Kurze Lebens-Notizen zu der Portrait-Gallerie merkwürdiger Luzerner auf der Bürgerbibliothek in Luzern</a></small></blockquote> 
        ${relPersInfo}  
        <h4>Links</h4>
          <p><small><a href="https://n2t.net/${zentralGutData.ark}">ZentralGut</a>${wpUrl} &bull; <a href="https://www.wikidata.org/entity/${portrait.QID}">Wikidata-Item</a> ${hlsIdUrl}${rzsUrl}</small></p>`;
        // Populate the Wikidata information section
        wikidataInfoDiv.innerHTML = wikidataHTML;
        
        const portraitImg = document.getElementById("largePortrait"); // Replace with the actual ID of your portrait image element
        console.log(imgElement);
        portraitImg.src = imgElement.src;
        miradorUrl = `https://zentralgut.ch/mirador/?manifest=https://zentralgut.ch/api/v1/records/${vorgangsTitel}/manifest&same_viewer=true`
        //document.getElementById("largePortraitMirador").href = "#";
        document.getElementById("largePortraitMirador").setAttribute("data-attr",vorgangsTitel);
        const portraitWidth = portraitImg.offsetWidth;
        wikidataInfoDiv.style.maxWidth = portraitWidth-20 + 'px';
        return true;
    }
  } catch (error) {
    console.error('Error:', error);
  } finally {
    // Hide the loading spinner
    clearTimeout(loadingTimeout);  // Cancel the timeout
    document.getElementById('loading-spinner').style.display = 'none';
  }
}


function loadImageAsync(url) {
  return new Promise((resolve, reject) => {
      const img = new Image();
      img.onload = () => resolve(img);
      img.onerror = reject;
      img.src = url;
  });
}


document.addEventListener("DOMContentLoaded", function() {
  // Your code here
  document.getElementById("prevButton").addEventListener("click", () => {
    // Your function here
    if (currentPortraitIndex > 0) {
      currentPortraitIndex--; // Decrement index
    }
    else { currentPortraitIndex = currentPortraitData.length -1;}
    updatePortrait(currentPortraitData);
  });
  // Event Listener for Next Button
  document.getElementById("nextButton").addEventListener("click", () => {
  if (currentPortraitIndex < currentPortraitData.length - 1) {
    currentPortraitIndex++; // Increment index
  }
  else { currentPortraitIndex = 0; }
  updatePortrait(currentPortraitData);
  console.log(currentPortraitIndex);
});
});


async function fetchFromMETS(url) {
  const response = await fetch(url);
  const xmlText = await response.text();

  const parser = new DOMParser();
  const xmlDoc = parser.parseFromString(xmlText, "text/xml");
  
  const namespaceResolver = {
    lookupNamespaceURI(prefix) {
      const ns = {
        'mods': 'http://www.loc.gov/mods/v3'
      };
      return ns[prefix] || null;
    }
  };
  
  const noteNode = xmlDoc.evaluate('//mods:note[@type="biographical/historical"]', xmlDoc, namespaceResolver, XPathResult.ANY_TYPE, null).iterateNext();
  const identifierNode = xmlDoc.evaluate('//mods:identifier[@type="ark"]', xmlDoc, namespaceResolver, XPathResult.ANY_TYPE, null).iterateNext();
  
  const result = {};

  if (noteNode) {
    result.text = noteNode.textContent;
    result.href = noteNode.getAttribute('href');
  }
  
  if (identifierNode) {
    result.ark = identifierNode.textContent;
  }
  
  return result;
}


class SPARQLQueryDispatcher {
  constructor(endpoint) {
      this.endpoint = endpoint;
  }

  query(sparqlQuery) {
      const fullUrl = this.endpoint + '?query=' + encodeURIComponent(sparqlQuery);
      const headers = { 'Accept': 'application/sparql-results+json' };

      return fetch(fullUrl, { headers }).then(body => body.json());
  }
}

async function runSPARQLQuery(QID) {
  const endpointUrl = 'https://query.wikidata.org/sparql';
  const sparqlQuery = `PREFIX target: <http://www.wikidata.org/entity/${QID}>
  SELECT ?label ?desc ?dod ?dodPrecision ?dob ?dobPrecision ?poDLabel ?poBLabel ?relLabel ?orderLabel ?hlsId ?gndId ?wpPage ?child ?childLabel ?father ?fatherLabel ?mother ?motherLabel ?relative ?relativeLabel ?kinshipLabel ?occupColl ?occupCollLabel (GROUP_CONCAT(?occupLabel; separator=", ") AS ?occupation)  WHERE {
    target: rdfs:label ?label.
    FILTER((LANG(?label)) = "de")
    target: schema:description ?desc.
    FILTER((LANG(?desc)) = "de")
    #Biographische Daten - Geburtsort/datum, Sterbeortdatum, Beruf, Religion, rel. Orden
    OPTIONAL { target: p:P569/psv:P569[wikibase:timePrecision ?dobPrecision; wikibase:timeValue ?dob] }
    OPTIONAL { target: p:P570/psv:P570[wikibase:timePrecision ?dodPrecision; wikibase:timeValue ?dod] }
    OPTIONAL { target: wdt:P19 ?poB. ?poB rdfs:label ?poBLabel. FILTER (LANG(?poBLabel) = "de")}
    OPTIONAL { target: wdt:P20 ?poD. ?poD rdfs:label ?poDLabel. FILTER (LANG(?poDLabel) = "de")}
    OPTIONAL { target: wdt:P106 ?occup.}
    OPTIONAL { target: wdt:P140 ?rel. ?rel rdfs:label ?relLabel. FILTER (LANG(?relLabel) = "de") }
    OPTIONAL { target: wdt:P611 ?order. ?order rdfs:label ?orderLabel. FILTER (LANG(?orderLabel) = "de") }  
    #OPTIONAL { target: wdt:P39 ?position. ?position rdfs:label ?positionLabel. FILTER (LANG(?positionLabel) = "de")}  
    # Identifier, Wikipedia
    OPTIONAL { target: wdt:P902 ?hlsId. }  
    OPTIONAL { target: wdt:P227 ?gndId. }  
    OPTIONAL { ?wpPage schema:about target:; schema:isPartOf <https://de.wikipedia.org/> }
    # Verwandte die ebenfalls in der Porträtgalerie hängen
    OPTIONAL { target: wdt:P40 ?child. ?child wdt:P1343 wd:Q112578667. ?child rdfs:label ?childLabel. FILTER (LANG(?childLabel) = "de") }
    OPTIONAL { target: wdt:P22 ?father. ?father wdt:P1343 wd:Q112578667. ?father rdfs:label ?fatherLabel. FILTER (LANG(?fatherLabel) = "de")  }
    OPTIONAL { target: wdt:P25 ?mother. ?mother wdt:P1343 wd:Q112578667. ?mother rdfs:label ?motherLabel. FILTER (LANG(?motherLabel) = "de")}
    ## Andere Verwandte
    OPTIONAL {
      target: wdt:P1343 wd:Q112578667;
              p:P1038 ?relativeStmt. 
      ?relativeStmt ps:P1038 ?relative;
                  pq:P1039 ?kinship. 
      ?relative  wdt:P1343 wd:Q112578667. 
      ?relative rdfs:label ?relativeLabel. FILTER(LANG(?relativeLabel)="de"). 
      ?kinship rdfs:label ?kinshipLabel. FILTER(LANG(?kinshipLabel)="de").
    }
    # Personen der Galerie mit gleicher Occupation
    OPTIONAL { ?occupColl wdt:P106 ?occup. 
               FILTER(?occupColl != target:)
               ?occupColl wdt:P1343 wd:Q112578667. 
               ?occupColl rdfs:label ?occupCollLabel. FILTER (LANG(?occupCollLabel) = "de")
               ?occup rdfs:label ?occupLabel. FILTER (LANG(?occupLabel) = "de")}
  }
  GROUP BY ?label ?desc ?dod ?dodPrecision ?dob ?dobPrecision ?poDLabel ?poBLabel ?relLabel ?orderLabel ?hlsId ?gndId ?wpPage ?child ?childLabel ?father ?fatherLabel ?mother ?motherLabel ?relative ?relativeLabel ?kinshipLabel ?occupColl ?occupCollLabel`;

  const queryDispatcher = new SPARQLQueryDispatcher(endpointUrl);
  const result = await queryDispatcher.query(sparqlQuery); // Using await here
  return result;
}

function transformIsoDate(isoDate, Precision) {
  console.log(Precision);
  if (isoDate) {
    const date = new Date(isoDate);
    const day = String(date.getDate()).padStart(2, '0');
    const month = String(date.getMonth() + 1).padStart(2, '0'); // Months are 0-based in JavaScript
    const year = date.getFullYear();
    
    switch(Precision){
      case "11":
        return `${day}.${month}.${year}`;
        break;
      case "9":
        return `${year}`;
        break;
      case "10":
        return `${month}.${year}`;
        break;
    }
  }
}

async function findPortraitByQIDWithFrame(qidArray) {
  try {
    const response = await fetch('frames.json'); // Change the URL to match your setup
    if (!response.ok) {
      throw new Error('Failed to fetch JSON');
    }
    data = await response.json();
  } catch (error) {
    console.error('Error fetching JSON:', error);
    return;
  }
  let foundPortraits = [];

      // Loop through each frame
      for (const frameKey in data) {
        const portraits = data[frameKey].portraits;

        // Loop through each portrait in the current frame
        for (const portrait of portraits) {
            if (qidArray.includes(portrait.QID)) {
                foundPortraits.push({
                    frameKey,  // Parent frame key
                    portrait  // Portrait data
                });
            }
        }
    }

    return foundPortraits.length > 0 ? foundPortraits : null;
}

// Sample usage
/*
const qidsToSearch = ["Q19287770"];
findPortraitByQIDWithFrame(qidsToSearch).then(qidSearchResult => {
  console.log(qidSearchResult[0]);
});
*/
document.addEventListener("DOMContentLoaded", function() {
  document.body.addEventListener('click', function(event) {
      if (event.target.matches('.updateInfoBox')) {
          event.preventDefault();
          const dataId = event.target.getAttribute('data-id');
          console.log(dataId);
          const qidsToSearch = [];
          qidsToSearch.push(dataId);
          console.log(qidsToSearch);
          findPortraitByQIDWithFrame(qidsToSearch).then(qidSearchResult => {
            console.log(qidSearchResult[0].portrait);
            getInfobox(qidSearchResult[0].portrait);
            const portraitImage = document.getElementById("largePortrait");
            //portraitImage.src = "../images/"+qidSearchResult[0].portrait.portraitFile;
            portraitImage.src = qidSearchResult[0].portrait.fileUrl;
          });
      }
      // Event for show further OccupationColleagues
      if (event.target.matches('#showOccups')) {
          event.preventDefault();
          occupsDiv = document.getElementsByClassName("occups");
          for(var i = 0; i < occupsDiv.length; i++){
            occupsDiv[i].style.display = 'inline';
            document.getElementById("showOccupsLink").style.display = 'none';
          }
      }

      //Mirador-Click-Window-Open-Event (eigentlich nur nötig, wenn same_viewer=true Url-Param funktoinieren würde)
      if (event.target.matches('#largePortrait')){
        event.preventDefault();
        vorgangsTitel = document.getElementById("largePortraitMirador").getAttribute("data-attr");
        window.open("https://zentralgut.ch/mirador/?manifest=https://zentralgut.ch/api/v1/records/"+vorgangsTitel+"/manifest&same_viewer=true","ZentralGut-Mirador");
        //window.open("https://viewer.zb.uzh.ch/mv/index.php?manifest=https://zentralgut.ch/api/v1/records/"+vorgangsTitel+"/manifest&same_viewer=true","ZentralGut-Mirador");
      }
      
  });
});