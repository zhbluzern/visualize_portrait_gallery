// preloadImages.js

let images = [
    'Katalogsaal_00.jpg',
    'Katalogsaal_01.jpg',
    'Katalogsaal_02.jpg',
    'Katalogsaal_03.jpg',
    'Katalogsaal_04.jpg',
    'Katalogsaal_05.jpg',
    'Katalogsaal_06.jpg'
  ];
  
  let imageDimensions = {};
  
  function preloadAndCalculateDimensions() {
    let loadedImages = 0; // Keep track of how many images have loaded
    images.forEach((src) => {
      let img = new Image();
      img.onload = function() {
        imageDimensions[src] = {
          naturalWidth: this.naturalWidth,
          naturalHeight: this.naturalHeight
        };
        loadedImages++;
        if (loadedImages === images.length) {
          // All images have been loaded, you can now safely use imageDimensions
          console.log('All images preloaded', imageDimensions);
        }
      };
      img.src = `path/to/images/${src}`;
    });
  }
  
  // Run this function to start preloading and calculating
  preloadAndCalculateDimensions();
  