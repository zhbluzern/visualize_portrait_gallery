# Visualize Lucerne Portrait Gallery

Demo: https://zhbluzern.gitlab.io/visualize_portrait_gallery/src

## Description

### Gallery of remarkable people of Lucerne

In the library hall of the ZHB Lucerne you find 260 painted portraits (oil on canvas) owned by the "Korporation Luzern" about remarkable persons of the history of Lucerne (the so called "merkwürdige Luzernerinnen und Luzerner") The portraits are digitized and open access available via ZentralGut.ch, the repository for the digital cultural heritage of Central Switzerland hosted by ZHB Lucerne. 240 portraits are already available under public domain, 21 portraits are still under copyright and only available at ZentralGut. 

### Biographies about the "merkwürdige Luzerner:innen"

Along the portraits the library has a collection of short biographical notes about each portraited person. The so called "Kurze Lebens-Notizen zu der Portrait-Gallerie merkwürdiger Luzerner auf der Bürgerbibliothek in Luzern" are digital available at ZentralGut(https://n2t.net/ark:/63274/zhb1309s) but furthermore we made a double-checked [fulltext transcript using WikiSource](https://de.wikisource.org/wiki/Kurze_Lebens-Notizen_zu_der_Portrait-Gallerie_merkw%C3%BCrdiger_Luzerner_auf_der_B%C3%BCrgerbibliothek_in_Luzern) as crowdsourcing platform. These corrected biographical notes are stored also in the portraits metadata at ZentralGut and made the portraits a little bit more retrievable. 

### Wikidata-fication of the potrait gallery

Every portraited person has already its own Wikidata-Item. These items has different statements, according to the data already stated in wikidata or available using different sources, starting with the short biographical notes. The following [Query](https://w.wiki/7Q73) returns all biographical items, with image and URL into the Wikisource transcript of the biographical note. 

### Portraits uploaded to Wikimedia Commons

The 240 portraits under public domain are already uploaded to [Wikimedia Commons](https://commons.wikimedia.org/wiki/Category:Portr%C3%A4tgalerie_der_merkw%C3%BCrdigen_Luzernerinnen_und_Luzerner) and linked to the wikidata item of the described person, unless there was already a satisfied image stated. The addition or exchange of images in different Wikipedias is a currently ongoing task (2023-09).

## Challenges

ZHB Luzern provides a more or less fully "wikified" dataset of biographical information, a digitized biographical source, interoperable and linked biographical items and decent portaits of the so-called remarkable people from Lucerne. According to the different statements avaialble in Wikidata a lot of different queries could be build and those group of people therefore analyzed. E.g. search for relative, people became the same political position, member of the same religious order enz.

* Buidling a variety of analysis (and visualisations) on the biographical informations of the portraited persons
* Add more granular information about differnet people (e.g. political parties, religion, relatives) or links to external sources
* Think about a (3-d) virtual visualization of the library hall and its portrait gallery.

## Links

* [Portrait Gallery at ZentralGut](https://zentralgut.ch/merkwuerdige_Luzerner/)
* [Kurze Lebensnotizen... at ZentralGut](https://n2t.net/ark:/63274/zhb1309s) = [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Kurze_Lebens-Notizen_zu_der_Portrait-Gallerie_merkw%C3%BCrdiger_Luzerner_auf_der_B%C3%BCrgerbibliothek_in_Luzern.pdf)  = [Wikisource](https://de.wikisource.org/wiki/Kurze_Lebens-Notizen_zu_der_Portrait-Gallerie_merkw%C3%BCrdiger_Luzerner_auf_der_B%C3%BCrgerbibliothek_in_Luzern)
* [Wikidata - Poträtgalerie - Project Overview](https://www.wikidata.org/wiki/User:ZentralGut/Portr%C3%A4tgalerie)
* [Wikidata Query with images and link to Wikisource biographical note](https://w.wiki/7Q73)
* [Wikimedia Commons Category:Porträtgalerie der merkwüridgen...](https://commons.wikimedia.org/wiki/Category:Portr%C3%A4tgalerie_der_merkw%C3%BCrdigen_Luzernerinnen_und_Luzerner)